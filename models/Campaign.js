const { Schema, model, Types } = require("mongoose");
const schema = new Schema({
    campaignName: {
      type: String,
      required: true,
    },
    discountType: {
      type: String,
      enum: ["BOGO Discount", "Price-Based Discount", "Time-Based Discount"],
      required: true,
    },
    validTill: {
      type: Date,
      required: true,
    },
    numOfDays: [
      {
        type: Schema.Types.Mixed,
      },
    ],
    itemCategories: {
      type: String,
      enum: [
        "Burritoos",
        "Vegeterian Burritos",
        "Breakfast",
        "Tacos",
        "Salads",
        "Special Plates",
        "Beverages",
        "Nachos",
        "Sandwiches and Burgers",
        "Top Menu Items",
        "Side Orders",
        "Quesadillas",
        "Soups",
        "Desserts",
        "Tortas",
      ],
      default: "All",
    },
    minItemCount: [
      {
        type: Schema.Types.Mixed,
      },
    ],
    discountPrice: [
      {
        type: Schema.Types.Mixed,
      },
    ],
    totalRedemptions: {
      type: Number,
    },
    isCategoryMenuOpen: {
      type: Boolean,
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
    updatedAt: {
      type: Date,
      default: Date.now,
    },
});
module.exports = model("Campaign", schema);
